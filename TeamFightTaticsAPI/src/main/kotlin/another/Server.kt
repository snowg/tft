import com.google.gson.Gson
import java.io.BufferedReader
import java.io.InputStreamReader
import java.math.BigInteger
import java.net.HttpURLConnection
import java.net.URL

fun main(args: Array<String>){
    get()
}


fun get(){

    val key = "RGAPI-0724ac65-026d-4cad-9baa-d3d805f0fc77"
    val urlTeste = "https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/RiotSchmick?api_key=$key"

    val url = URL(urlTeste)

    val connection = url.openConnection() as HttpURLConnection

    connection.requestMethod = "GET"

    val responseCode = connection.responseCode

    println(responseCode)

    val inp = BufferedReader(InputStreamReader(connection.inputStream))

    val response = StringBuffer()

    var inputLine = inp.readLine()

    while (inputLine != null){
        response.append(inputLine)
        inputLine = inp.readLine()
    }

    inp.close()

    println(response)

    val gson = Gson()
    val noname = gson.fromJson(response.toString(),SummunerModel::class.java)
    println(noname.accountId)



}

data class SummunerModel(
    val id: String,
    val accountId: String,
    val puuid: String,
    val name: String,
    val profileIconId: String,
    val revisionDate: BigInteger,
    val summonerLevel: Int
)