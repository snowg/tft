package br.snowg.teamfighttatics

import br.snowg.teamfighttatics.requests.SoloMid
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TeamFightTaticsApplication

fun main(args: Array<String>) {
	val x = SoloMid()
	x.getAllItems()
	runApplication<TeamFightTaticsApplication>(*args)
}
