package br.snowg.teamfighttatics.configuration

import com.mongodb.ConnectionString
import com.mongodb.reactivestreams.client.MongoClient
import com.mongodb.reactivestreams.client.MongoClients
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import com.mongodb.MongoClientURI
import com.mongodb.ServerAddress
import com.mongodb.connection.netty.NettyStreamFactoryFactory


//@Configuration
//@EnableReactiveMongoRepositories(basePackages = ["br.snowg.teamfighttatics"])
//class MongoConfig: AbstractReactiveMongoConfiguration() {
//
//    @Value("\${port}")
//    private lateinit var port: String
//
//    @Value("\${dbName}")
//    private lateinit var dbName: String
//
//    var uri = MongoClientURI("mongodb+srv://snowg:<password>@cluster0-n47sg.mongodb.net/test?retryWrites=true&w=majority")
//
//    override fun reactiveMongoClient(): MongoClient {
//        return MongoClients.create("mongodb+srv://snowg:<password>@cluster0-n47sg.mongodb.net/test?retryWrites=true&w=majority" )
//    }
//
//    override fun getDatabaseName(): String {
//        return dbName
//    }
//
//    @Bean
//    @Primary
//    override fun reactiveMongoTemplate(): ReactiveMongoTemplate {
//        return ReactiveMongoTemplate(reactiveMongoClient(), databaseName)
//    }
//}