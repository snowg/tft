package br.snowg.teamfighttatics.controller

import br.snowg.teamfighttatics.model.view.ChampionView
import br.snowg.teamfighttatics.view.champion.IChampionViewHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
class ChampionController {

    @Autowired
    private lateinit var championHandler: IChampionViewHandler

    @GetMapping(value = ["/champions"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getAllChampions(): Flux<ChampionView> {
        return championHandler.listAllChampionsViews()
    }

    @GetMapping("/champion/{championID}")
    fun getByID(@PathVariable(value = "championID") id : String): Mono<ChampionView> {
        return championHandler.getChampionViewByID(id)
    }

    @DeleteMapping(value = ["/champion/{championID}"])
    fun deleteChampion(@PathVariable(value = "championID") id : String): Mono<Boolean> {
        return championHandler.deleteChampionView(id)
    }

    @PutMapping(value = ["champion"])
    fun alterChampion(@Valid @RequestBody champion: ChampionView): Mono<ChampionView> {
        return championHandler.alterChampionView(champion)
    }

    @PostMapping("/champion")
    fun createChampion(@Valid @RequestBody champion: ChampionView): Mono<ChampionView> {
        return championHandler.createChampionView(champion = champion)
    }

    @PostMapping(value = ["/champion/populate"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun populateChampions(): Flux<ChampionView> {
        return championHandler.populateSoloMidChampions()
    }
}