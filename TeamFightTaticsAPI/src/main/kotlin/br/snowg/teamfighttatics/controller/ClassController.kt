package br.snowg.teamfighttatics.controller

import br.snowg.teamfighttatics.view.classes.IClassViewHandler
import br.snowg.teamfighttatics.model.view.ClassView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
class ClassController {
    @Autowired
    private lateinit var classHandler: IClassViewHandler

    @GetMapping(value = ["/classes"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getAllClasses(): Flux<ClassView> {
        return classHandler.listAllClassViews()
    }

    @GetMapping("/class/{classID}")
    fun getByID(@PathVariable(value = "classID") id : String): Mono<ClassView> {
        return classHandler.getClassViewByID(id)
    }

    @DeleteMapping(value = ["/class/{classID}"])
    fun deleteClass(@PathVariable(value = "classID") id : String): Mono<Boolean> {
        return classHandler.deleteClassView(id)
    }

    @PutMapping(value = ["/class"])
    fun alterClass(@Valid @RequestBody classe: ClassView): Mono<ClassView> {
        return classHandler.alterClassView(classe)
    }

    @PostMapping("/class")
    fun createClass(@Valid @RequestBody classe: ClassView): Mono<ClassView> {
        return classHandler.createClassView(classe)
    }

    @PostMapping(value = ["/class/populate"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun populateOrigins(): Flux<ClassView>{
        return classHandler.populateSoloMidClass()
    }
}