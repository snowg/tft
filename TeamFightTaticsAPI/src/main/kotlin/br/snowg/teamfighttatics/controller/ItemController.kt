package br.snowg.teamfighttatics.controller

import br.snowg.teamfighttatics.model.view.ItemView
import br.snowg.teamfighttatics.view.item.IItemViewHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
class ItemController {

    @Autowired
    private lateinit var handler: IItemViewHandler

    @GetMapping(value = ["/items"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getAllItems(): Flux<ItemView> {
        return handler.listAllItemsViews()
    }

    @GetMapping("/item/{itemID}")
    fun getByID(@PathVariable(value = "itemID") id : String): Mono<ItemView> {
        return handler.getItemViewByID(id)
    }

    @DeleteMapping(value = ["/item/{itemID}"])
    fun deleteItem(@PathVariable(value = "itemID") id : String): Mono<Boolean> {
        return handler.deleteItemView(id)
    }

    @PutMapping(value = ["/item"])
    fun alterItem(@Valid @RequestBody Item: ItemView): Mono<ItemView> {
        return handler.alterItemView(Item)
    }

    @PostMapping("/item")
    fun createItem(@Valid @RequestBody item: ItemView): Mono<ItemView> {
        return handler.createItemView(item = item)
    }

    @GetMapping(value = ["/item/populate"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun populateItems(): Flux<ItemView> {
        return handler.populateSoloMidItems()
    }
}