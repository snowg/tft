package br.snowg.teamfighttatics.controller

import br.snowg.teamfighttatics.model.view.OriginView
import br.snowg.teamfighttatics.view.origin.IOriginViewHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
class OriginController {

    @Autowired
    private lateinit var originHandler: IOriginViewHandler

    @GetMapping(value = ["/origins"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getAllOrigins(): Flux<OriginView> {
        return originHandler.listAllOriginsViews()
    }

    @GetMapping("/origin/{originID}")
    fun getByID(@PathVariable(value = "originID") id : String): Mono<OriginView> {
        return originHandler.getOriginViewByID(id)
    }

    @DeleteMapping(value = ["/origin/{originID}"])
    fun deleteOrigin(@PathVariable(value = "originID") id : String): Mono<Boolean>{
        return originHandler.deleteOriginView(id)
    }

    @PutMapping(value = ["/origin"])
    fun alterOrigin(@Valid @RequestBody origin: OriginView): Mono<OriginView>{
        return originHandler.alterOriginView(origin)
    }

    @PostMapping("/origin")
    fun createOrigin(@Valid @RequestBody origin: OriginView): Mono<OriginView>{
        return originHandler.createOriginView(origin = origin)
    }

    @PostMapping(value = ["/origin/populate"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun populateOrigins(): Flux<OriginView>{
        return originHandler.populateSoloMidOrigins()
    }

}