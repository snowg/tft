package br.snowg.teamfighttatics.handlers.champion

import br.snowg.teamfighttatics.model.database.Champion
import br.snowg.teamfighttatics.repository.ChampionRepository
import br.snowg.teamfighttatics.requests.SoloMid
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ChampionHandler: IChampionHandler {

    @Autowired
    private lateinit var repo: ChampionRepository

    override fun listAllChampions(): Flux<Champion> {
        return repo.findAll()
    }

    override fun createChampion(newChampion: Champion): Mono<Champion> {
        return repo.save(newChampion)
    }

    override fun alterChampion(newChampion: Champion): Mono<Champion> {
        return repo.save(newChampion)
    }

    override fun deleteChampion(championID: ObjectId): Mono<Boolean> {
        return repo.findById(championID.toString()).flatMap {
            repo.delete(it).then(Mono.just(true))
        }.defaultIfEmpty(false)
    }

    override fun listChampionByID(championID: ObjectId): Mono<Champion> {
        return repo.findById(championID.toString())
    }

    override fun populateSoloMidChampion(): Flux<Champion> {
        val soloMid = SoloMid()
        val flux = Flux.fromIterable(soloMid.getAllChampions())
        return flux.flatMap { repo.save(it) }
    }
}