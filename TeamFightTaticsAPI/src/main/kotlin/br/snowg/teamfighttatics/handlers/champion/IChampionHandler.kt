package br.snowg.teamfighttatics.handlers.champion

import br.snowg.teamfighttatics.model.database.Champion
import br.snowg.teamfighttatics.model.database.Class
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IChampionHandler {
    fun listAllChampions(): Flux<Champion>

    fun createChampion(newChampion: Champion): Mono<Champion>

    fun alterChampion(newChampion: Champion): Mono<Champion>

    fun deleteChampion(championID: ObjectId): Mono<Boolean>

    fun listChampionByID(championID: ObjectId): Mono<Champion>

    fun populateSoloMidChampion(): Flux<Champion>
}