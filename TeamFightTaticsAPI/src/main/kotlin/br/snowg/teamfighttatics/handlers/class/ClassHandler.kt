package br.snowg.teamfighttatics.handlers.`class`

import br.snowg.teamfighttatics.model.database.Class
import br.snowg.teamfighttatics.repository.ClassRepository
import br.snowg.teamfighttatics.requests.SoloMid
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ClassHandler: IClassHandler {

    @Autowired
    private lateinit var repo: ClassRepository

    override fun listAllClasses(): Flux<Class> {
        return repo.findAll()
    }

    override fun createClass(newClass: Class): Mono<Class> {
        return repo.save(newClass)
    }

    override fun alterClass(newClass: Class): Mono<Class> {
        return repo.save(newClass)
    }

    override fun deleteClass(classID: ObjectId): Mono<Boolean> {
        return repo.findById(classID.toString()).flatMap { existingClass ->
            repo.delete(existingClass)
                    .then(Mono.just(true))
        }.defaultIfEmpty(false)
    }

    override fun listClassByID(classID: ObjectId): Mono<Class> {
        return repo.findById(classID.toString())
    }

    override fun populateSoloMidClass(): Flux<Class> {
        val soloMid = SoloMid()
        val flux = Flux.fromIterable(soloMid.getAllClasses())
        return flux.flatMap { repo.save(it) }
    }
}