package br.snowg.teamfighttatics.handlers.`class`

import br.snowg.teamfighttatics.model.database.Class
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IClassHandler {
    fun listAllClasses(): Flux<Class>

    fun createClass(newClass: Class): Mono<Class>

    fun alterClass(newClass: Class): Mono<Class>

    fun deleteClass(classID: ObjectId): Mono<Boolean>

    fun listClassByID(classID: ObjectId): Mono<Class>

    fun populateSoloMidClass(): Flux<Class>
}