package br.snowg.teamfighttatics.handlers.item

import br.snowg.teamfighttatics.model.database.Item
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IItemHandler {
    fun listAllItems(): Flux<Item>

    fun createItem(item: Item): Mono<Item>

    fun alterItem(newItem: Item): Mono<Item>

    fun deleteItem(itemID: ObjectId): Mono<Boolean>

    fun listItemByID(itemID: ObjectId): Mono<Item>

    fun populateSoloMidItems(): Flux<Item>
}