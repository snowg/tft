package br.snowg.teamfighttatics.handlers.item

import br.snowg.teamfighttatics.model.database.Item
import br.snowg.teamfighttatics.repository.ItemRepository
import br.snowg.teamfighttatics.requests.SoloMid
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ItemHandler: IItemHandler {

    @Autowired
    private lateinit var repo: ItemRepository

    override fun listAllItems(): Flux<Item> {
        return repo.findAll()
    }

    override fun createItem(item: Item): Mono<Item> {
        return repo.save(item)
    }

    override fun alterItem(newItem: Item): Mono<Item> {
        return repo.save(newItem)
    }

    override fun deleteItem(itemID: ObjectId): Mono<Boolean> {
        return repo.findById(itemID.toString()).flatMap { existingClass ->
            repo.delete(existingClass)
                    .then(Mono.just(true))
        }.defaultIfEmpty(false)
    }

    override fun listItemByID(itemID: ObjectId): Mono<Item> {
        return repo.findById(itemID.toString())
    }

    override fun populateSoloMidItems(): Flux<Item> {
        val soloMid = SoloMid()
        val flux = Flux.fromIterable(soloMid.getAllItems())
        return flux.flatMap { repo.save(it) }
    }
}