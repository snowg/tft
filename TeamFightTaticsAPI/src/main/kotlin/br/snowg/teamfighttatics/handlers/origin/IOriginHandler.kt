package br.snowg.teamfighttatics.handlers.origin

import br.snowg.teamfighttatics.model.database.Origin
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IOriginHandler {

    fun listAllOrigins(): Flux<Origin>

    fun createOrigin(origin: Origin): Mono<Origin>

    fun alterOrigin(newOrigin: Origin): Mono<Origin>

    fun deleteOrigin(originID: ObjectId): Mono<Boolean>

    fun listOriginByID(originID: ObjectId): Mono<Origin>

    fun populateSoloMidOrigins(): Flux<Origin>
}