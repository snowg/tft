package br.snowg.teamfighttatics.handlers.origin

import br.snowg.teamfighttatics.model.database.Origin
import br.snowg.teamfighttatics.repository.OriginRepository
import br.snowg.teamfighttatics.requests.SoloMid
import org.bson.types.ObjectId
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class OriginHandler: IOriginHandler {

    companion object {
        private val logger = LoggerFactory.getLogger(IOriginHandler::class.java)
    }

    @Autowired
    private lateinit var originRepository: OriginRepository

    override fun createOrigin(origin: Origin): Mono<Origin> {
        return originRepository.save(origin)
    }

    override fun alterOrigin(newOrigin: Origin): Mono<Origin> {
        return originRepository.save(newOrigin)
    }


    override fun deleteOrigin(originID: ObjectId): Mono<Boolean> {
        return originRepository.findById(originID.toString()).flatMap { existingOrigin ->
            originRepository.delete(existingOrigin)
                    .then(Mono.just(true))
        }.defaultIfEmpty(false)
    }

    override fun listAllOrigins(): Flux<Origin> {
        return originRepository.findAll()
    }

    override fun listOriginByID(originID: ObjectId): Mono<Origin> {
        return originRepository.findById(originID.toString())
    }

    override fun populateSoloMidOrigins(): Flux<Origin> {
        val soloMid = SoloMid()
        val flux = Flux.fromIterable(soloMid.getAllOrigins())
        return flux.flatMap { originRepository.save(it) }
    }
}