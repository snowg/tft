package br.snowg.teamfighttatics.model.common

data class Ability(
        val name: String?,
        val description: String?,
        val type: AbilityType?,
        val manaCost: Int?,
        val manaStart: Int?,
        val stats: List<AbilityStates>?

)