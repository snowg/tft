package br.snowg.teamfighttatics.model.common

data class AbilityStates(
        val type: String?,
        val value: String?
)