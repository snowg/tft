package br.snowg.teamfighttatics.model.common

enum class AbilityType {
    Active,
    Passive
}