package br.snowg.teamfighttatics.model.common

data class Bonus(
        val needed: Int?,
        val effect: String?
)