package br.snowg.teamfighttatics.model.common

data class ChampionStatus(
    val offense: OffenseStats?,
    val defense: DefenseStats?
)