package br.snowg.teamfighttatics.model.common

data class DefenseStats(
        val health: Int?,
        val armor: Int?,
        val magicResist: Int?
)