package br.snowg.teamfighttatics.model.common

enum class ItemKind {
    basic,
    advanced
}