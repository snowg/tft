package br.snowg.teamfighttatics.model.common

data class ItemStats(
        val name: String?,
        val title: String?,
        val amount: String?
)