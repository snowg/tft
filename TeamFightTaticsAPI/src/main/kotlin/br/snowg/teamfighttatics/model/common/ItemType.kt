package br.snowg.teamfighttatics.model.common

enum class ItemType {
    Utility,
    Defensive,
    Offensive
}