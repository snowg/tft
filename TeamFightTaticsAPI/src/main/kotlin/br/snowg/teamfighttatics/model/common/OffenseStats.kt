package br.snowg.teamfighttatics.model.common

data class OffenseStats(
        val damage: Int?,
        val attackSpeed: Double?,
        val dps: Int?,
        val range: Int?
)