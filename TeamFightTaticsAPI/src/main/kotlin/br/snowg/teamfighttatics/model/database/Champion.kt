package br.snowg.teamfighttatics.model.database

import br.snowg.teamfighttatics.model.common.Ability
import br.snowg.teamfighttatics.model.common.ChampionStatus
import br.snowg.teamfighttatics.model.view.ChampionView
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "champions")
data class Champion(
        @Id
        val id: ObjectId?,
        val mockID: String?,
        val key: String?,
        val name: String?,
        val origin: List<String>?,
        val classes : List<String>?,
        val cost: Int?,
        val ability: Ability?,
        val stats : ChampionStatus?,
        val items: List<String>?
){
    fun toChampionView(): ChampionView {
        return ChampionView(
                id = this.id.toString(),
                mockID = this.mockID,
                key = this.key,
                name = this.name,
                origin = this.origin,
                classes = this.classes,
                cost = this.cost,
                ability = this.ability,
                stats = this.stats,
                items = this.items
        )
    }
}