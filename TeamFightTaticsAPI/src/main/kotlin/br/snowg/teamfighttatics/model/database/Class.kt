package br.snowg.teamfighttatics.model.database
import br.snowg.teamfighttatics.model.common.Bonus
import br.snowg.teamfighttatics.model.view.ClassView
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "classes")
data class Class(
        @Id
        val id: ObjectId?,
        val key: String?,
        val name: String?,
        val description: String?,
        val accentChampionImage: String?,
        val bonuses: List<Bonus>
) {
    fun toClassView(): ClassView {
        return ClassView(
                id = this.id.toString(),
                key = this.key,
                name = this.name,
                description = this.description,
                accentChampionImage = this.accentChampionImage,
                bonuses = this.bonuses
        )
    }
}