package br.snowg.teamfighttatics.model.database

import br.snowg.teamfighttatics.model.common.ItemKind
import br.snowg.teamfighttatics.model.common.ItemStats
import br.snowg.teamfighttatics.model.common.ItemType
import br.snowg.teamfighttatics.model.view.ItemView
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "items")
data class Item (
        @Id val id: ObjectId?,
        val key: String?,
        val name: String?,
        val type: ItemType?,
        val bonus: String?,
        val tier: Int?,
        val depth: Int?,
        val stats: List<ItemStats>?,
        val kind: ItemKind?,
        val buildsFrom: List<String>?,
        val buildsInto: List<String>?,
        val champs: List<String>?
){
    fun toItemView():ItemView {
        return ItemView(
                id = this.id.toString(),
                key = this.key,
                name = this.name,
                type = this.type,
                bonus = this.bonus,
                tier = this.tier,
                depth = this.depth,
                stats = this.stats,
                kind = this.kind,
                buildsFrom = this.buildsFrom,
                buildsInto = this.buildsInto,
                champs = this.champs
        )
    }
}