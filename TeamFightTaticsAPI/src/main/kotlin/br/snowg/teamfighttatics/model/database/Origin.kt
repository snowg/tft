package br.snowg.teamfighttatics.model.database

import br.snowg.teamfighttatics.model.common.Bonus
import br.snowg.teamfighttatics.model.view.OriginView
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "origins")
data class Origin (
    @Id
    val id: ObjectId?,

    //@NotBlank
    //@Size(max = 20)
    val key: String?,

    val name: String?,

    //@NotBlank
    //@Size(max = 255)
    val description: String?,

    //@Size(max = 255)
    val accentChampionImage: String?,

    val bonuses: List<Bonus>?

//    @NotNull
//    val createdWhen: Instant
){
    fun toOriginView(): OriginView {
        return OriginView(
                id = this.id.toString(),
                key = this.key,
                name = this.name,
                description = this.description,
                accentChampionImage = this.accentChampionImage,
                bonuses = this.bonuses
        )
    }
}