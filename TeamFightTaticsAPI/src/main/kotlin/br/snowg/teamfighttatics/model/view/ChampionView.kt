package br.snowg.teamfighttatics.model.view

import br.snowg.teamfighttatics.model.common.Ability
import br.snowg.teamfighttatics.model.common.ChampionStatus
import br.snowg.teamfighttatics.model.database.Champion
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id

data class ChampionView(
        val id: String? = null,
        val mockID: String? = null,
        val key: String? = null,
        val name: String? = null,
        val origin: List<String>? = null,
        val classes : List<String>? = null,
        val cost: Int? = null,
        val ability: Ability? = null,
        val stats : ChampionStatus? = null,
        val items: List<String>? = null
){
    fun toChampion(): Champion{
        return Champion(
                id = if (this.id != null) {
                    ObjectId(this.id)
                } else {
                    null
                },
                mockID = this.mockID,
                key = this.key,
                name = this.name,
                origin = this.origin,
                classes = this.classes,
                cost = this.cost,
                ability = this.ability,
                stats = this.stats,
                items = this.items
        )
    }
}