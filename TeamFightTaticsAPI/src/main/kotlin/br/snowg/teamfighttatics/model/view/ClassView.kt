package br.snowg.teamfighttatics.model.view

import br.snowg.teamfighttatics.model.common.Bonus
import br.snowg.teamfighttatics.model.database.Class
import org.bson.types.ObjectId

data class ClassView(
        val id: String? = null,
        val key: String? = null,
        val name: String? = null,
        val description: String? = null,
        val accentChampionImage: String? = null,
        val bonuses: List<Bonus>
){
    fun toClass(): Class {
        return Class(
                id = if (this.id != null) {
                    ObjectId(this.id)
                } else {
                    null
                },
                name = this.name,
                key = this.key,
                description = this.description,
                accentChampionImage = this.accentChampionImage,
                bonuses = this.bonuses
        )
    }

}