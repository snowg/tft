package br.snowg.teamfighttatics.model.view

import br.snowg.teamfighttatics.model.common.ItemKind
import br.snowg.teamfighttatics.model.common.ItemStats
import br.snowg.teamfighttatics.model.common.ItemType
import br.snowg.teamfighttatics.model.database.Item
import org.bson.types.ObjectId

data class ItemView(
        val id: String? = null,
        val key: String? = null,
        val name: String? = null,
        val type: ItemType? = null,
        val bonus: String? = null,
        val tier: Int? = null,
        val depth: Int? = null,
        val stats: List<ItemStats>? = null,
        val kind: ItemKind? = null,
        val buildsFrom: List<String>? = null,
        val buildsInto: List<String>? = null,
        val champs: List<String>? = null
){
    fun toItem():Item {
        return Item(
                id = if (this.id != null) {
                    ObjectId(this.id)
                } else {
                    null
                },
                key = this.key,
                name = this.name,
                type = this.type,
                bonus = this.bonus,
                tier = this.tier,
                depth = this.depth,
                stats = this.stats,
                kind = this.kind,
                buildsFrom = this.buildsFrom,
                buildsInto = this.buildsInto,
                champs = this.champs
        )
    }
}