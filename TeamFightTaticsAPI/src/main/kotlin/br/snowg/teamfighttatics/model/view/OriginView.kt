package br.snowg.teamfighttatics.model.view

import br.snowg.teamfighttatics.model.common.Bonus
import br.snowg.teamfighttatics.model.database.Origin
import org.bson.types.ObjectId


data class OriginView(
        val id: String? = null,
        val key: String? = null,
        val name: String? = null,
        val description: String? = null,
        val accentChampionImage: String? = null,
        val bonuses: List<Bonus>? = null
){
    fun toOrigin(): Origin {
        return Origin(
                id = if (this.id != null) {
                    ObjectId(this.id)
                } else {
                    null
                },
                name = this.name,
                key = this.key,
                description = this.description,
                accentChampionImage = this.accentChampionImage,
                bonuses = this.bonuses
        )
    }
}