package br.snowg.teamfighttatics.repository

import br.snowg.teamfighttatics.model.database.Champion
import br.snowg.teamfighttatics.model.database.Class
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ChampionRepository: ReactiveMongoRepository<Champion, String>