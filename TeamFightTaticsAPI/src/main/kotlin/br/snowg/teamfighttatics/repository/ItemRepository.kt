package br.snowg.teamfighttatics.repository

import br.snowg.teamfighttatics.model.database.Item
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ItemRepository: ReactiveMongoRepository<Item, String>