package br.snowg.teamfighttatics.repository

import br.snowg.teamfighttatics.model.database.Origin
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface OriginRepository: ReactiveMongoRepository<Origin,String>