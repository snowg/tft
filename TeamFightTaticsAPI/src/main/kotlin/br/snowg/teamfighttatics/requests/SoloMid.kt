package br.snowg.teamfighttatics.requests

import br.snowg.teamfighttatics.model.database.Champion
import br.snowg.teamfighttatics.model.database.Origin
import br.snowg.teamfighttatics.model.database.Class
import br.snowg.teamfighttatics.model.database.Item
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

import java.net.URL

class SoloMid {
    private val originURI = "https://solomid-resources.s3.amazonaws.com/blitz/tft/data/origins.json"
    private val classURI = "https://solomid-resources.s3.amazonaws.com/blitz/tft/data/classes.json"
    private val itemsURI = "https://solomid-resources.s3.amazonaws.com/blitz/tft/data/items.json"
    private val championsURI = "https://solomid-resources.s3.amazonaws.com/blitz/tft/data/champions.json"
    private  val tierListURI = "https://solomid-resources.s3.amazonaws.com/blitz/tft/data/tierlist.json"

    fun getAllOrigins(): List<Origin> {
        val returnValue = URL(originURI).readText()
        val originMap: Map <String, Origin> = jacksonObjectMapper().readValue(returnValue)
        return originMap.map { it.value }
    }

    fun getAllClasses(): List<Class>{
        val returnValue = URL(classURI).readText()
        val originMap: Map <String, Class> = jacksonObjectMapper().readValue(returnValue)
        return originMap.map { it.value }
    }

    fun getAllChampions() : List<Champion> {
        val returnValue = URL(championsURI).readText()
        val filteredValue = returnValue.replace("\"id\":","\"mockID\":").replace("\"class\":","\"classes\":")
        val originMap: Map<String, Champion> = jacksonObjectMapper().readValue(filteredValue)
        return originMap.map { it.value }
    }

    fun getAllItems(): List<Item>{
        val returnValue = URL(itemsURI).readText()
        val originMap: Map<String, Item> = jacksonObjectMapper().readValue(returnValue)
        return originMap.map { it.value }
    }

}