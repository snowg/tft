package br.snowg.teamfighttatics.view.champion

import br.snowg.teamfighttatics.handlers.champion.IChampionHandler
import br.snowg.teamfighttatics.model.view.ChampionView
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ChampionViewHandler: IChampionViewHandler {

    @Autowired
    private lateinit var handler: IChampionHandler

    override fun listAllChampionsViews(): Flux<ChampionView> {
        return handler.listAllChampions().map { champion -> champion.toChampionView() }
    }

    override fun createChampionView(champion: ChampionView): Mono<ChampionView> {
        return handler.createChampion(newChampion = champion.toChampion()).map { it.toChampionView() }
    }

    override fun alterChampionView(newChampionView: ChampionView): Mono<ChampionView> {
        return handler.alterChampion(newChampion = newChampionView.toChampion()).map { it.toChampionView() }
    }

    override fun deleteChampionView(championID: String): Mono<Boolean> {
        return handler.deleteChampion(championID = ObjectId(championID))
    }

    override fun getChampionViewByID(championID: String): Mono<ChampionView> {
        return handler.listChampionByID(ObjectId(championID)).map { it.toChampionView() }
    }

    override fun populateSoloMidChampions(): Flux<ChampionView> {
        return handler.populateSoloMidChampion().map { it.toChampionView() }
    }


}