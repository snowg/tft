package br.snowg.teamfighttatics.view.champion

import br.snowg.teamfighttatics.model.view.ChampionView
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IChampionViewHandler {
    // List all saved Champions on view model
    fun listAllChampionsViews(): Flux<ChampionView>

    // Create a new Champion based on view model
    fun createChampionView(champion: ChampionView): Mono<ChampionView>

    // Alter a created Champion based on view model
    fun alterChampionView(newChampionView: ChampionView): Mono<ChampionView>

    // Delete a Champion based on view model
    fun deleteChampionView(championID: String): Mono<Boolean>

    // List ChampionView with a id
    fun getChampionViewByID(championID: String): Mono<ChampionView>

    fun populateSoloMidChampions(): Flux<ChampionView>
}