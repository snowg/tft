package br.snowg.teamfighttatics.view.classes

import br.snowg.teamfighttatics.handlers.`class`.IClassHandler
import br.snowg.teamfighttatics.model.view.ClassView
import org.bson.types.ObjectId
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ClassViewHandler : IClassViewHandler {

    companion object {
        private val logger = LoggerFactory.getLogger(ClassViewHandler::class.java)
    }

    @Autowired
    private lateinit var classHandler: IClassHandler


    override fun listAllClassViews(): Flux<ClassView> {
        logger.info("Received a new request to list classes.")
        return classHandler.listAllClasses().map { item -> item.toClassView() }.doOnNext {
            logger.info("Class ${it.id} successfully listed.")
        }.doOnError {
            logger.error("Failed to listen classes.Error: ${it.message}")
        }
    }

    override fun createClassView(newClassView: ClassView): Mono<ClassView> {
        return classHandler.createClass(newClassView.toClass()).map { it.toClassView() }.doOnNext {
            logger.info("Class ${it.id} created successfully.")
        }.doOnError {
            logger.error("Failed to create a class. Error: ${it.message}")
        }
    }

    override fun deleteClassView(classsID: String): Mono<Boolean> {
        logger.info("Received a new class delete request.")
        return classHandler.deleteClass(ObjectId(classsID)).doOnNext {
            logger.info("Class $classsID deleted successfully.")
        }.doOnError {
            logger.error("Failed to delete class $classsID. Error: ${it.message}")
        }
    }

    override fun getClassViewByID(classID: String): Mono<ClassView> {
        return classHandler.listClassByID(classID = ObjectId(classID)).map { it.toClassView() }.doOnNext {
            logger.info("Class ${it.id} listed  successfully.")
        }.doOnError {
            logger.error("Failed to listen a class $classID. Error: ${it.message}")
        }
    }

    override fun alterClassView(newClassView: ClassView): Mono<ClassView> {
        return classHandler.alterClass(newClass = newClassView.toClass()).map { it.toClassView() }.doOnNext {
            logger.info("Class ${it.id} changed  successfully.")
        }.doOnError {
            logger.error("Failed to change a class ${newClassView.id}. Error: ${it.message}")
        }
    }

    override fun populateSoloMidClass(): Flux<ClassView> {
        return classHandler.populateSoloMidClass().map { it.toClassView() }.doOnNext {
            logger.info("Class ${it.id} populated  successfully.")
        }.doOnError {
            logger.error("Failed to populate. Error: ${it.message}")
        }
    }

}