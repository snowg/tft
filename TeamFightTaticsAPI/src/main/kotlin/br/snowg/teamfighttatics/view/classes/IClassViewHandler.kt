package br.snowg.teamfighttatics.view.classes

import br.snowg.teamfighttatics.model.view.ClassView
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IClassViewHandler {
    // List all saved origins on view model
    fun listAllClassViews(): Flux<ClassView>

    // Create a new Origin based on view model
    fun createClassView(newClassView: ClassView): Mono<ClassView>

    // Alter a created origin based on view model
    fun alterClassView(newClassView: ClassView): Mono<ClassView>

    // Delete a origin based on view model
    fun deleteClassView(classsID: String): Mono<Boolean>

    // List originview with a id
    fun getClassViewByID(classID: String): Mono<ClassView>

    fun populateSoloMidClass(): Flux<ClassView>
}