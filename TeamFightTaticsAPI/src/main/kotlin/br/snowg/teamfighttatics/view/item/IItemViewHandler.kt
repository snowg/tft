package br.snowg.teamfighttatics.view.item

import br.snowg.teamfighttatics.model.view.ItemView
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IItemViewHandler {
    
    // List all saved Items on view model
    fun listAllItemsViews(): Flux<ItemView>

    // Create a new Item based on view model
    fun createItemView(item: ItemView): Mono<ItemView>

    // Alter a created Item based on view model
    fun alterItemView(newItemView: ItemView): Mono<ItemView>

    // Delete a Item based on view model
    fun deleteItemView(itemID: String): Mono<Boolean>

    // List ItemView with a id
    fun getItemViewByID(itemID: String): Mono<ItemView>

    fun populateSoloMidItems(): Flux<ItemView>
}