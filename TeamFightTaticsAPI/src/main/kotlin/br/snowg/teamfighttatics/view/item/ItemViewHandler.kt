package br.snowg.teamfighttatics.view.item

import br.snowg.teamfighttatics.handlers.item.IItemHandler
import br.snowg.teamfighttatics.model.view.ItemView
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ItemViewHandler: IItemViewHandler {

    @Autowired
    lateinit var handler: IItemHandler

    override fun listAllItemsViews(): Flux<ItemView> {
        return handler.listAllItems().map { it.toItemView() }
    }

    override fun createItemView(item: ItemView): Mono<ItemView> {
        return handler.createItem(item = item.toItem()).map { it.toItemView() }
    }

    override fun alterItemView(newItemView: ItemView): Mono<ItemView> {
        return handler.alterItem(newItem = newItemView.toItem()).map { it.toItemView() }
    }

    override fun deleteItemView(itemID: String): Mono<Boolean> {
        return handler.deleteItem(ObjectId(itemID))
    }

    override fun getItemViewByID(itemID: String): Mono<ItemView> {
        return handler.listItemByID(ObjectId(itemID)).flatMap { Mono.just(it.toItemView()) }
    }

    override fun populateSoloMidItems(): Flux<ItemView> {
        return handler.populateSoloMidItems().flatMap { Mono.just(it.toItemView()) }
    }
}