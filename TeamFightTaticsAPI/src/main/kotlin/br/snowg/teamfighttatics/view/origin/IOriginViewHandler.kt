package br.snowg.teamfighttatics.view.origin

import br.snowg.teamfighttatics.model.view.OriginView
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IOriginViewHandler {

    // List all saved origins on view model
    fun listAllOriginsViews(): Flux<OriginView>

    // Create a new Origin based on view model
    fun createOriginView(origin: OriginView): Mono<OriginView>

    // Alter a created origin based on view model
    fun alterOriginView(newOriginView: OriginView): Mono<OriginView>

    // Delete a origin based on view model
    fun deleteOriginView(originID: String): Mono<Boolean>

    // List originview with a id
    fun getOriginViewByID(originID: String): Mono<OriginView>

    fun populateSoloMidOrigins(): Flux<OriginView>
}