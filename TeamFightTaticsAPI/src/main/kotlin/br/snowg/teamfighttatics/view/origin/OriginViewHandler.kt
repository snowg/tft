package br.snowg.teamfighttatics.view.origin

import br.snowg.teamfighttatics.handlers.origin.IOriginHandler
import br.snowg.teamfighttatics.model.view.OriginView
import org.bson.types.ObjectId
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class OriginViewHandler: IOriginViewHandler {

    companion object {
        private val logger = LoggerFactory.getLogger(IOriginViewHandler::class.java)
    }

    @Autowired
    private lateinit var originHandler: IOriginHandler

    override fun listAllOriginsViews(): Flux<OriginView> {
        logger.info("Received a new request to list origins.")
        return originHandler.listAllOrigins().map { item -> item.toOriginView() }.doOnNext {
            logger.info("Origin ${it.id} successfully listed.")
        }.doOnError {
            logger.error("Failed to listen origins.Error: ${it.message}")
        }
    }

    override fun deleteOriginView(originID: String): Mono<Boolean> {
        logger.info("Received a new origin delete request.")
        return originHandler.deleteOrigin(ObjectId(originID)).doOnNext {
            logger.info("Origin $originID deleted successfully.")
        }.doOnError {
            logger.error("Failed to delete origin $originID. Error: ${it.message}")
        }
    }

    override fun createOriginView(origin: OriginView): Mono<OriginView> {
        return originHandler.createOrigin(origin.toOrigin()).map { it.toOriginView() }.doOnNext {
            logger.info("Origin ${it.id} created successfully.")
        }.doOnError {
            logger.error("Failed to create a origin. Error: ${it.message}")
        }
    }

    override fun alterOriginView(newOriginView: OriginView): Mono<OriginView> {
        return originHandler.alterOrigin(newOrigin = newOriginView.toOrigin()).map { it.toOriginView() }.doOnNext {
            logger.info("Origin ${it.id} changed  successfully.")
        }.doOnError {
            logger.error("Failed to change a origin ${newOriginView.id}. Error: ${it.message}")
        }
    }

    override fun getOriginViewByID(originID: String): Mono<OriginView> {
        return originHandler.listOriginByID(originID = ObjectId(originID)).map { it.toOriginView() }.doOnNext {
            logger.info("Origin ${it.id} listed  successfully.")
        }.doOnError {
            logger.error("Failed to listen a origin $originID. Error: ${it.message}")
        }
    }

    override fun populateSoloMidOrigins(): Flux<OriginView> {
        return originHandler.populateSoloMidOrigins().map { it.toOriginView() }.doOnNext {
            logger.info("Origin ${it.id} populated  successfully.")
        }.doOnError {
            logger.error("Failed to populate a origin. Error: ${it.message}")
        }
    }

}
